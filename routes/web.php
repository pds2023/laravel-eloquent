<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->middleware('auth');

Route::get('/register', [AuthController::class, 'register'])->middleware('auth');

Route::post('/welcome', [AuthController::class, 'welcome'])->middleware('auth');

Route::get('/welcome', [AuthController::class, 'welcome'])->middleware('auth');

Route::get('/data-tables',[IndexController::class, 'datatables'])->middleware('auth');

Route::resource('cast', CastController::class)->middleware('auth');

Route::resource('game', GameController::class)->middleware('auth');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
