<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Game</h2>

{{-- //Code disini --}}
<form action="/game/{{$game->id}}" method="post">

    @csrf
    @method('PUT')

    <label for="name">Name : </label>
    <input type="text" class="form-control" id="name" name="name" value="{{$game->name}}"><br><br>
    @error('name')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="gameplay">Gameplay</label><br>
    <textarea class="form-control" name="gameplay" id="gameplay" cols="30" rows="10">{{$game->gameplay}}</textarea><br><br>
    @error('gameplay')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="developer">Developer : </label>
    <input type="text" class="form-control" id="developer" name="developer" value="{{$game->developer}}"><br><br>
    @error('developer')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="year">Year : </label>
    <input type="number" id="year" class="form-control" name="year" min=1800 max=2100 value="{{$game->year}}"><br><br>
    @error('year')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <input type="submit" class="btn btn-primary" value="Update Game">

</form>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>