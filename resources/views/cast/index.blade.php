@extends('layout.master')

@section('sitetitle','Dashboard')

@section('title')
Cast Table
@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush


@section('content')
<a href="{{route('cast.create')}}" class="btn btn-primary">Add Cast</a><br><br>
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>ID</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key=>$value)
    <tr>
      <td>{{ $key+1 }}</td>
      <td>{{ $value->nama }}</td>
      <td>{{ $value->umur }}</td>
      <td>{{ $value->bio }}</td>
      <td>
        <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
        <a href="/cast/{{$value->id}}/edit" class="btn btn-success">Edit</a>
        <form action="/cast/{{$value->id}}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @empty
        <tr>
            <td>No data</td>
        </tr>  
    @endforelse   
    
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Aksi</th>
    </tr>
    </tfoot>
  </table>
  @endsection
