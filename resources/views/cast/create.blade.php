@extends('layout.master')

@section('sitetitle','Dashboard')

@section('title')
Add Cast
@endsection

@section('content')
<form action="/cast" method="post">

    @csrf

    <label for="nama">Nama : </label>
    <input type="text" class="form-control" id="nama" name="nama"><br><br>
    @error('nama')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="umur">umur : </label>
    <input type="number" id="umur" class="form-control" name="umur" min=17 max=100><br><br>
    @error('umur')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="bio">Bio</label><br>
    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
    @error('bio')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <input type="submit" class="btn btn-primary" value="Add Cast">

</form>
@endsection
