@extends('layout.master')

@section('sitetitle','Dashboard')

@section('title')
Show Cast id {{$cast->id}}
@endsection

@section('content')
<h4>Nama : {{$cast->nama}}</h4>
<p>Umur : {{$cast->umur}}</p>
<h4>Bio :</h4>
<p>{{$cast->bio}}</p>
@endsection